package telegram

import (
	"log"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
)

type telegramPusher struct {
	bot *tgbotapi.BotAPI
}

func NewTelegramPusher(botToken string) telegramPusher {
	var t telegramPusher

	var err error

	t.bot, err = tgbotapi.NewBotAPI(botToken)
	if err != nil {
		log.Println("Error initializing telegram")
		log.Panic(err)
		return t
	}

	//t.bot.Debug = true

	log.Printf("Authorized on account %s", t.bot.Self.UserName)

	return t
}

func (t *telegramPusher) Push(channel, msg string) error {
	msgToSend := tgbotapi.NewMessageToChannel(channel, msg)
	log.Println("Sending to telegram")
	if t.bot == nil {
		log.Println("Cannot push, telegram API not initialized")
		return nil
	}
	_, err := t.bot.Send(msgToSend)
	if err != nil {
		log.Println(err)
		return err
	}

	return nil
}
