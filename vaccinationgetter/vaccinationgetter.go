package vaccinationgetter

import (
	"encoding/json"
	"log"
	"net/http"
	"net/url"
)

type Vaccinationgetter struct {
	Plz                    string
	IsAppointmentAvailable bool
	AvailableAppointment   VaccinationResult
}

type VaccinationCompleteResult struct {
	Result []VaccinationResult `json:"resultList"`
}

type VaccinationResult struct {
	Name              string `json:"name"`
	OutOfStock        bool   `json:"outOfStock"`
	NumberOfOpenSlots int    `json:"freeSlotSizeOnline"`
	VaccineName       string `json:"vaccineName"`
}

func (v *Vaccinationgetter) LoadVaccinationStatus() {

	baseURL := "https://www.impfportal-niedersachsen.de/portal/rest/appointments/findVaccinationCenterListFree/"

	params := url.Values{}

	params.Add("count", "1")
	params.Add("birthdate", "-3600000") //1.1.1970

	client := http.Client{}
	req, _ := http.NewRequest("GET", baseURL+v.Plz, nil)
	req.URL.RawQuery = params.Encode()
	req.Header.Add("Accept", "*/*")

	resp, err := client.Do(req)

	if err == nil {
		defer resp.Body.Close()
	}

	if err == nil && resp.StatusCode == 200 {
		var parsedResult VaccinationCompleteResult

		json.NewDecoder(resp.Body).Decode(&parsedResult)

		VaccinationResult := getFirstAvailableVaccination(parsedResult)
		if VaccinationResult != nil {
			v.IsAppointmentAvailable = true
			v.AvailableAppointment = *VaccinationResult
			log.Println(parsedResult)
		} else {
			v.IsAppointmentAvailable = false
		}
		return

	} else {
		log.Printf("Unable to get status for %v.\n", v.Plz)
		log.Println(resp)
	}

}

func getFirstAvailableVaccination(v VaccinationCompleteResult) *VaccinationResult {
	for _, x := range v.Result {
		if !x.OutOfStock {
			return &x
		}
	}

	return nil
}
