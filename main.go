package main

import (
	"flag"
	"fmt"
	"log"
	"time"

	"gitlab.com/chri.koch/vaccinator/config"
	"gitlab.com/chri.koch/vaccinator/telegram"
	"gitlab.com/chri.koch/vaccinator/vaccinationgetter"
)

type VaccinationTransmitState struct {
	v                      vaccinationgetter.Vaccinationgetter
	lastRunFreeAppointment bool
	TelegramChannel        string
}

func main() {
	var ConfigFilename string

	flag.StringVar(&ConfigFilename, "config", "config.json", "filename of config-file")
	flag.Usage()
	flag.Parse()

	myConfig, err := config.New(ConfigFilename)
	if err != nil {
		log.Printf("Error reading config: %v\n", err)
		return
	}

	log.Println(myConfig)

	t := telegram.NewTelegramPusher(myConfig.TelegramBotToken)

	vaccinationStates := []VaccinationTransmitState{}
	for _, c := range myConfig.Channels {
		for _, p := range c.PLZs {
			//init lastRunFreeAppointment to true, because we don't want to send messages after startup
			vaccinationStates = append(vaccinationStates,
				VaccinationTransmitState{v: vaccinationgetter.Vaccinationgetter{Plz: p},
					lastRunFreeAppointment: true, TelegramChannel: c.TelegramChannelName})
		}
	}

	for {
		for k := range vaccinationStates {
			vaccinationStates[k].v.LoadVaccinationStatus()

			if vaccinationStates[k].v.IsAppointmentAvailable {
				msg := fmt.Sprintf("Es gibt %v freie Impftermine im %v mit Impfstoff %v.", vaccinationStates[k].v.AvailableAppointment.NumberOfOpenSlots, vaccinationStates[k].v.AvailableAppointment.Name, vaccinationStates[k].v.AvailableAppointment.VaccineName)
				//log.Println(msg)
				if vaccinationStates[k].lastRunFreeAppointment == false && vaccinationStates[k].v.AvailableAppointment.NumberOfOpenSlots >= myConfig.MinFreeSlots {
					//inform about new free appointments
					pusherr := t.Push(vaccinationStates[k].TelegramChannel, msg)
					if pusherr == nil {
						//only update if pushing succeeded
						//in case of errer assume we didn't post the message so we'll try again
						vaccinationStates[k].lastRunFreeAppointment = true
					}

				}
			} else {
				//log.Printf("Nothing available for %v :-(\n", vaccinationStates[k].v.Plz)
				if vaccinationStates[k].lastRunFreeAppointment == true {
					log.Printf("No more appointements for %v.\n", vaccinationStates[k].v.Plz)
				}
				vaccinationStates[k].lastRunFreeAppointment = false

			}

		}

		time.Sleep(time.Second * time.Duration(myConfig.IntervalSeconds))
	}

}
